#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/../locals/mysqldump_params.sh

if [[ ! -z $2   ]]
then
  MYSQL_DATABASE=$2
fi

DUMP_FOLDER=$DUMP_FOLDER/$MYSQL_DATABASE
DUMP_FILE=$MYSQL_DATABASE

#allow overide folder 
if [[ ! -z $3   ]]
then
  DUMP_FOLDER=$3
  echo $DUMP_FOLDER
fi


if [[ -z $DUMP_FOLDER || -z $DUMP_FILE || -z $MYSQL_USER || -z $MYSQL_DATABASE   ]]
then
    echo "insert params in locals/mysqldump_params.sh"
    exit;
fi



mysql_dump_hourly()
{
    hourly_folder=$DUMP_FOLDER/hourly
    folder=$hourly_folder/`date +"%Y_%m_%d"`
    file=`date +"%Y_%m_%d_%H"`.$DUMP_FILE.sql
    mysql_dump_to_file $folder $file

    #Delete directories older than 7 days 
    #find $hourly_folder -type d -mtime +7 | xargs rm -rf
}


mysql_dump_daily()
{
    daily_folder=$DUMP_FOLDER/daily
    folder=$daily_folder/`date +"%Y_%m"`
    file=`date +"%Y_%m_%d"`.$DUMP_FILE.sql

    mysql_dump_to_file $folder $file

    #Delete directories older than 4 monthes
    find $daily_folder -type d -mtime +123 | xargs rm -rf
}

mysql_dump_weekly()
{
    folder=$DUMP_FOLDER/weekly
    file=`date +"%Y_%m_%d"`.$DUMP_FILE.sql
    mysql_dump_to_file $folder $file
}

mysql_dump_to_file()
{
    folder=$1
    file=$1/$2
    mkdir -p $folder
    mysqldump --opt --user=${MYSQL_USER} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE} > ${file}
    gzip -f $file;
}

case "$1" in 
    hourly)
       mysql_dump_hourly
        ;;
     
    daily)
        mysql_dump_daily
        ;;
    weekly)
        mysql_dump_weekly
        ;;
    *)
        echo "Usage: $0 {hourly|daily|weekly}"
        exit 1
esac

