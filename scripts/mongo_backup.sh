#!/bin/bash
MONGO_DATABASE="parties"
APP_NAME="rocket_chat"

MONGO_HOST="127.0.0.1"
MONGO_PORT="27017"
TIMESTAMP=`date +%F-%H%M`
MONGODUMP_PATH="/snap/rocketchat-server/1324/bin/mongodump"
BACKUPS_DIR="/home/ubuntu/backups/$APP_NAME"
BACKUP_NAME="$TIMESTAMP-$MONGO_DATABASE"

# mongo admin --eval "printjson(db.fsyncLock())"
# $MONGODUMP_PATH -h $MONGO_HOST:$MONGO_PORT -d $MONGO_DATABASE
$MONGODUMP_PATH -d $MONGO_DATABASE
# mongo admin --eval "printjson(db.fsyncUnlock())"

mkdir -p $BACKUPS_DIR
mv dump $BACKUP_NAME
tar -zcvf $BACKUPS_DIR/$BACKUP_NAME.tgz $BACKUP_NAME
rm -rf $BACKUP_NAME

#rm files older than 30 days
find $BACKUPS_DIR -type f -mtime +30 -exec rm {} \;
