#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source $SCRIPT_DIR/../locals/postgres_params.sh

if [[ ! -z $2   ]]
then
  POSTGRES_DATABASE=$2
fi

DUMP_FOLDER=$DUMP_FOLDER/$POSTGRES_DATABASE
DUMP_FILE=$POSTGRES_DATABASE

#allow overide folder 
if [[ ! -z $3   ]]
then
  DUMP_FOLDER=$3
  echo $DUMP_FOLDER
fi


if [[ -z $DUMP_FOLDER || -z $DUMP_FILE || -z $POSTGRES_USER || -z $POSTGRES_DATABASE   ]]
then
    echo "insert params in locals/postgresdump_params.sh"
    exit;
fi


postgres_dump_manual()
{
    folder=$DUMP_FOLDER/manual
    file=`date +"%Y_%m_%d_%H"`.$DUMP_FILE.sql
    postgres_dump_to_file $folder $file
    ls $folder 
}

postgres_dump_hourly()
{
    hourly_folder=$DUMP_FOLDER/hourly
    folder=$hourly_folder/`date +"%Y_%m_%d"`
    file=`date +"%Y_%m_%d_%H"`.$DUMP_FILE.sql
    postgres_dump_to_file $folder $file

    #Delete directories older than 7 days 
    #find $hourly_folder -type d -mtime +7 | xargs rm -rf
}


postgres_dump_daily()
{
    daily_folder=$DUMP_FOLDER/daily
    folder=$daily_folder/`date +"%Y_%m"`
    file=`date +"%Y_%m_%d"`.$DUMP_FILE.sql

    postgres_dump_to_file $folder $file

    #Delete directories older than 4 monthes
    find $daily_folder -type d -mtime +123 | xargs rm -rf
}

postgres_dump_weekly()
{
    folder=$DUMP_FOLDER/weekly
    file=`date +"%Y_%m_%d"`.$DUMP_FILE.sql
    postgres_dump_to_file $folder $file
}

postgres_dump_to_file()
{
    folder=$1
    file=$1/$2
    mkdir -p $folder
    PGPASSWORD= ${POSTGRES_PASSWORD} pg_dump -U ${POSTGRES_USER} -h ${HOST} -W -C -d ${POSTGRES_DATABASE} > ${file}
    # gzip -f $file;
}

case "$1" in 
    manual)
       postgres_dump_manual
        ;;
    hourly)
       postgres_dump_hourly
        ;;
     
    daily)
        postgres_dump_daily
        ;;
    weekly)
        postgres_dump_weekly
        ;;
    *)
        echo "Usage: $0 {manual | hourly | daily | weekly}"
        exit 1
esac

