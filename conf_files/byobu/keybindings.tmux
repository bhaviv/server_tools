unbind-key -n C-a
set -g prefix ^A
set -g prefix2 ^A
bind a send-prefix

unbind-key -n M-Left
unbind-key -n M-Right
unbind-key -n M-Up
unbind-key -n M-Down

bind-key -n M-h previous-window
bind-key -n M-l next-window
bind-key -n M-j switch-client -p
bind-key -n M-k switch-client -n
