ALIASES_FOLDER=$SERVER_TOOLS_DIR
ALIASES_FILE=$ALIASES_FOLDER/aliases.sh
LOCAL_ALIASES_FILE=$ALIASES_FOLDER/locals/aliases.sh

alias ebs='vi $ALIASES_FILE'
alias sbs='source $ALIASES_FILE'
alias ebsl='vi $LOCAL_ALIASES_FILE'
if [ -e $LOCAL_ALIASES_FILE ]
then
  source $LOCAL_ALIASES_FILE
fi

#server tools
alias updateServerTools='git -C $SERVER_TOOLS_DIR pull'

alias vi='vim'
alias psg='ps -ef | grep'

alias ll='ls -la'

#hosts
alias ehosts="sudo vi /etc/hosts"

#remove files
alias rm='echo "use trash-put or \rm if you really want to delete it "; false'
alias tp='trash-put'
alias tl='trash-list'

# usefule grep
alias 'hg=history |grep'
alias 'alig=alias |grep'
alias 'envg=env |grep'

# check current computre ip 
alias 'myip=ifconfig |grep "inet addr" '

# ssh
alias ssh-my-public-key='cat ~/.ssh/id_rsa.pub'

#git
alias gst="git status"
alias gd="git diff"
alias gb="git branch"
alias gco="git checkout"

#laravel
alias art='php artisan'
alias artg='php artisan | grep '
alias artv='php artisan | vim -'

alias art-cache-clear='art cache:clear'
alias art-clear-compiled='art clear-compiled'
alias art-config-clear='art config:clear'
alias art-debugbar-clear='art debugbar:clear'
alias art-route-clear='art route:clear '
alias art-view-clear='art view:clear '
    
alias art-route-list='art route:list'

#alias art-migrate='art migrate'
#alias art-migrate-rollback='art migrate:rollback'
#alias art-migrate-refresh='art migrate:refresh'
alias art-migrate-status='art migrate:status'
alias tinker='art tinker'


#du
alias du-total="du -sh"
alias du-folder-size="du -h --max-depth=1 --time"
